import SendIcon from '@mui/icons-material/Send';
import RotateLeftIcon from '@mui/icons-material/RotateLeft';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';

export const defaultKeyStatus = {
  label: '',
  noMatch: false,
  semiMatch: false,
  perfectMatch: false,
  submitted: false
};

export const defaultKeyRowList = [
  [
    {
      ...defaultKeyStatus,
      label: 'Q',
      type: 'letter'
    },
    {
      ...defaultKeyStatus,
      label: 'W',
      type: 'letter'
    },
    {
      ...defaultKeyStatus,
      label: 'E',
      type: 'letter'
    },
    {
      ...defaultKeyStatus,
      label: 'R',
      type: 'letter'
    },
    {
      ...defaultKeyStatus,
      label: 'T',
      type: 'letter'
    },
    {
      ...defaultKeyStatus,
      label: 'Y',
      type: 'letter'
    },
    {
      ...defaultKeyStatus,
      label: 'U',
      type: 'letter'
    },
    {
      ...defaultKeyStatus,
      label: 'I',
      type: 'letter'
    },
    {
      ...defaultKeyStatus,
      label: 'O',
      type: 'letter'
    },
    {
      ...defaultKeyStatus,
      label: 'P',
      type: 'letter'
    }
  ],
  [
    {
      ...defaultKeyStatus,
      label: 'A',
      type: 'letter'
    },
    {
      ...defaultKeyStatus,
      label: 'S',
      type: 'letter'
    },
    {
      ...defaultKeyStatus,
      label: 'D',
      type: 'letter'
    },
    {
      ...defaultKeyStatus,
      label: 'F',
      type: 'letter'
    },
    {
      ...defaultKeyStatus,
      label: 'G',
      type: 'letter'
    },
    {
      ...defaultKeyStatus,
      label: 'H',
      type: 'letter'
    },
    {
      ...defaultKeyStatus,
      label: 'J',
      type: 'letter'
    },
    {
      ...defaultKeyStatus,
      label: 'K',
      type: 'letter'
    },
    {
      ...defaultKeyStatus,
      label: 'L',
      type: 'letter'
    }
  ],
  [
    {
      ...defaultKeyStatus,
      label: 'Reset',
      type: 'reset',
      icon: <RotateLeftIcon />
    },
    {
      ...defaultKeyStatus,
      label: 'Delete',
      type: 'delete',
      icon: <ArrowBackIcon />
    },
    {
      ...defaultKeyStatus,
      label: 'Z',
      type: 'letter'
    },
    {
      ...defaultKeyStatus,
      label: 'X',
      type: 'letter'
    },
    {
      ...defaultKeyStatus,
      label: 'C',
      type: 'letter'
    },
    {
      ...defaultKeyStatus,
      label: 'V',
      type: 'letter'
    },
    {
      ...defaultKeyStatus,
      label: 'B',
      type: 'letter'
    },
    {
      ...defaultKeyStatus,
      label: 'N',
      type: 'letter'
    },
    {
      ...defaultKeyStatus,
      label: 'M',
      type: 'letter'
    },
    {
      ...defaultKeyStatus,
      label: 'Submit',
      type: 'submit',
      icon: <SendIcon fontSize="small" />
    }
  ]
];
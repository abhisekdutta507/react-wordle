export const parse = (string = '') => {
  try {
    return JSON.parse(string);
  } catch (e) {
    return undefined;
  }
};

export const stringify = (object = {}) => {
  try {
    return JSON.stringify(object);
  } catch (e) {
    return undefined;
  }
};

import { createContext, memo, useContext, useState } from "react";

import { parse, stringify } from "../utils/parse";
import { random, cloneDeep } from "lodash";

import wordList, { defaultLetterRowList } from "../constants/wordList";
import { defaultKeyRowList, defaultKeyStatus } from "../constants/keyList";

import "./Wordle.css";

export const LetterCell = memo(
  ({
    label = "",
    noMatch = false,
    semiMatch = false,
    perfectMatch = false,
  }) => {
    // console.log('@developers - letter cell rendered');
    let className = "wordle-letter-cell";

    if (noMatch) {
      className += " noMatch";
    }

    if (semiMatch) {
      className += " semiMatch";
    }

    if (perfectMatch) {
      className += " perfectMatch";
    }

    return <div className={className}>{label}</div>;
  }
);

export const LetterRow = memo(({ row: _row = "" }) => {
  const row = parse(_row);
  return (
    <div className="wordle-letter-row">
      {row.map((letter, index) => (
        <LetterCell
          key={`wordle-letter-cell-component-${index}`}
          label={letter.label}
          noMatch={letter.noMatch}
          semiMatch={letter.semiMatch}
          perfectMatch={letter.perfectMatch}
          submitted={letter.submitted}
        />
      ))}
    </div>
  );
});

export const Puzzle = memo(({ letterRowList = [] }) => {
  return (
    <div className="wordle-puzzle-wrapper">
      {letterRowList.map((row, index) => (
        <LetterRow
          key={`wordle-letter-row-component-${index}`}
          row={stringify(row)}
        />
      ))}
    </div>
  );
});

export const KeyCell = memo(
  ({
    label = "",
    icon = undefined,
    type = "",
    noMatch = false,
    semiMatch = false,
    perfectMatch = false
  }) => {
    const { handleKeyPress } = useContext(WordleContext);
    let className = "wordle-key-cell";

    if (perfectMatch) {
      className += " perfectMatch";
    } else if (semiMatch) {
      className += " semiMatch";
    } else if (noMatch) {
      className += " noMatch";
    }

    return (
      <div
        className={className}
        onClick={handleKeyPress.bind(this, { label, type })}
      >
        {icon ?? label}
      </div>
    );
  }
);

export const KeyRow = memo(({ row = [] }) => {
  return (
    <div className="wordle-key-row">
      {row.map(({ label, icon, type, noMatch, semiMatch, perfectMatch }, index) => (
        <KeyCell
          key={`wordle-key-cell-component-${index}`}
          label={label}
          icon={icon}
          type={type}
          noMatch={noMatch}
          semiMatch={semiMatch}
          perfectMatch={perfectMatch}
        />
      ))}
    </div>
  );
});

export const Keyboard = memo(
  ({ keyRowList = [] }) => {
    return (
      <div className="wordle-keyboard-wrapper">
        {keyRowList.map((row, index) => (
          <KeyRow
            key={`wordle-key-row-component-${index}`}
            row={[...row]}
          />
        ))}
      </div>
    );
  }
);

export const Wordle = () => {
  const { letterRowList, keyRowList } = useContext(WordleContext);
  return (
    <div className="wordle-wrapper">
      <Puzzle letterRowList={letterRowList} />
      <Keyboard keyRowList={keyRowList} />
    </div>
  );
};

export const WordleContext = createContext({
  word: "",
  wordList: [],
  letterRowList: defaultLetterRowList,
  keyRowList: defaultKeyRowList,
  handleKeyPress: () => {},
});

export const WordleController = () => {
  const maxRowIndex = 5;
  const maxCellIndex = 4;
  const [word, setWord] = useState(
    (() => {
      const wordIndex = random(0, wordList.length);
      return wordList[wordIndex];
    })()
  );
  const [rowIndex, setRowIndex] = useState(0);
  const [cellIndex, setCellIndex] = useState(-1);
  const [letterRowList, setLetterRowList] = useState(cloneDeep(defaultLetterRowList));
  const [keyRowList, setKeyRowList] = useState(cloneDeep(defaultKeyRowList));
  const [freeze, setFreeze] = useState({ success: false, failure: false });

  const updateKeyRowList = (_keyRow = []) => {
    const _keyRowList = [...keyRowList];
    _keyRowList.forEach((keyRow) => {
      keyRow.forEach((key, index, _keyRowParam) => {
        const updatedKey = _keyRow.find((_key) => _key.label === key.label);
        if (updatedKey) {
          _keyRowParam[index] = {
            ...key,
            noMatch: key.noMatch || updatedKey?.noMatch,
            semiMatch: key.semiMatch || updatedKey?.semiMatch,
            perfectMatch: key.perfectMatch || updatedKey?.perfectMatch,
            submitted: key.submitted || updatedKey?.submitted,
          };
        }
      });
    });
    setKeyRowList(_keyRowList);
  };

  const insertLetter = (key = { label: "", type: "" }) => {
    const { alert } = window;
    if (freeze.failure || freeze.success) {
      return;
    }
    if (cellIndex >= maxCellIndex) {
      return alert(`No more field left in row ${rowIndex}`);
    }
    // rest of the functions
    const _letterRowList = [...letterRowList];
    _letterRowList[rowIndex][cellIndex + 1] = {
      ...key,
      noMatch: false,
      semiMatch: false,
      perfectMatch: false,
      submitted: false,
    };
    setLetterRowList(_letterRowList);
    setCellIndex(cellIndex + 1);
  };

  const deleteLetter = () => {
    if (freeze.failure || freeze.success) {
      return;
    }
    if (cellIndex === -1) {
      return;
    }
    // rest of the functions
    const _letterRowList = [...letterRowList];
    _letterRowList[rowIndex][cellIndex] = { ...defaultKeyStatus };
    setLetterRowList(_letterRowList);
    setCellIndex(cellIndex - 1);
  };

  const handleSubmit = () => {
    const { alert } = window;
    if (freeze.failure || freeze.success) {
      return;
    }
    if (cellIndex !== maxCellIndex) {
      return alert("Guess is too small");
    }

    const wordArray = word.toUpperCase().split("");
    const guessedWordArray = letterRowList[rowIndex].map(({ label }) => label);
    const guessedWord = guessedWordArray.join("");

    const guessedWordIndex = wordList.findIndex(
      (w) => w === guessedWord.toLowerCase()
    );
    if (guessedWordIndex === -1) {
      return alert("Word doesn's exist");
    }

    const nextLetterRow = guessedWordArray.map((label, index) => {
      let noMatch = false;
      let semiMatch = false;
      let perfectMatch = false;
      const submitted = true;
      const guessedIndex = wordArray.indexOf(label);

      switch (guessedIndex) {
        case -1: {
          noMatch = true;
          break;
        }
        case index: {
          perfectMatch = true;
          break;
        }
        default: {
          semiMatch = true;
          break;
        }
      }

      return { label, noMatch, semiMatch, perfectMatch, submitted };
    });

    const _letterRowList = [...letterRowList];
    _letterRowList[rowIndex] = nextLetterRow;
    setLetterRowList(_letterRowList);
    updateKeyRowList(nextLetterRow);

    if (guessedWord.toLowerCase() === word) {
      setFreeze({ success: true, failure: false });
      return setTimeout(
        () =>
          alert(
            `Congratulations!! you got the word: ${word.toUpperCase()} right.`
          ),
        100
      );
    }

    if (rowIndex === maxRowIndex) {
      setFreeze({ success: false, failure: true });
      return setTimeout(
        () => alert(`Sorry!! the correct word is ${word.toUpperCase()}.`),
        100
      );
    }

    setCellIndex(-1);
    setRowIndex(rowIndex + 1);
  };

  const handleReset = () => {
    const confirmAction = window.confirm('Are you sure want to reset?');
    if (!confirmAction) {
      return;
    }

    // reset all the states
    const wordIndex = random(0, wordList.length);
    setWord(wordList[wordIndex]);
    setRowIndex(0);
    setCellIndex(-1);
    setLetterRowList(cloneDeep(defaultLetterRowList));
    setKeyRowList(cloneDeep(defaultKeyRowList));
    setFreeze({ success: false, failure: false });
  };

  const handleKeyPress = (key = { label: "", type: "" }) => {
    switch (key.type) {
      case 'letter': {
        insertLetter(key);
        break;
      }
      case 'delete': {
        deleteLetter();
        break;
      }
      case 'submit': {
        handleSubmit();
        break;
      }
      case 'reset': {
        handleReset();
        break;
      }
      default: {
        break;
      }
    }
  };

  return (
    <WordleContext.Provider
      value={{
        letterRowList,
        keyRowList,
        rowIndex,
        maxRowIndex,
        cellIndex,
        maxCellIndex,
        handleKeyPress,
      }}
    >
      <Wordle />
    </WordleContext.Provider>
  );
};

export default WordleController;

import Wordle from './components/Wordle';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        
      </header>

      <main>
        <Wordle />
      </main>

      <footer>
        Get the React.js codes <a href="https://bitbucket.org/abhisekdutta507/react-wordle/src/master/" target="_blank">here</a>.
      </footer>
    </div>
  );
}

export default App;
